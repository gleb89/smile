from pydantic import BaseSettings




class Settings(BaseSettings):

    """
    Хранит в себе данные настроек с валидацией 
    Pydantic
    """
    
    mail_username:str = None
    mail_password:str = None
    

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"


settings: Settings = Settings()