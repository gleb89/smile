import smtplib

from fastapi import FastAPI, APIRouter,BackgroundTasks
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel


from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


from conf.settings import settings

app = FastAPI(
    openapi_url="/api/v1/openapi.json",
    docs_url="/api/v1/docs",
)

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost:3000",
    "http://localhost", 
    "https://valencia-dental.es",
    "http://valencia-dental.es"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

email_router = APIRouter(
    prefix="/api/v1/email",
    tags=["email"],
)

def send_message_mail(email,message):
    you = email
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "Smile House"
    msg['From'] = settings.mail_username
    msg['To'] = email
    part2 = MIMEText(message, 'html')
    msg.attach(part2)
    mail = smtplib.SMTP('smtp.mail.ru', 587)
    mail.ehlo()
    mail.starttls()
    mail.login(settings.mail_username, settings.mail_password)
    mail.sendmail(settings.mail_username, email, msg.as_string())
    mail.quit()

class CreateEmail(BaseModel):
    name:str
    phone:str
    email:str
    
@email_router.get('')
def home():
    return 'userrrrrrrrrr'

@email_router.post('')
async def create_one(email: CreateEmail,background_tasks: BackgroundTasks):
    style = """
            * {
                margin: 0;
                padding: 0;
                font-size: 100%;
                font-family: 'Avenir Next', "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
                line-height: 1.65; }
                img {
                max-width: 100%;
                margin: 0 auto;
                display: block; }
                body,
                .body-wrap {
                width: 100% !important;
                height: 100%;
                background: #efefef;
                -webkit-font-smoothing: antialiased;
                -webkit-text-size-adjust: none; }
                a {
                color: #71bc37;
                text-decoration: none; }
                .text-center {
                text-align: center; }
                .text-right {
                text-align: right; }
                .text-left {
                text-align: left; }
                .button {
                display: inline-block;
                color: white;
                background: #71bc37;
                border: solid #71bc37;
                border-width: 10px 20px 8px;
                font-weight: bold;
                border-radius: 4px; }
                h1, h2, h3, h4, h5, h6 {
                margin-bottom: 20px;
                line-height: 1.25; }
                h1 {
                font-size: 32px; }
                h2 {
                font-size: 28px; }
                h3 {
                font-size: 24px; }
                h4 {
                font-size: 20px; }
                h5 {
                font-size: 16px; }
                p, ul, ol {
                font-size: 16px;
                font-weight: normal;
                margin-bottom: 20px; }
                .container {
                display: block !important;
                clear: both !important;
                margin: 0 auto !important;
                max-width: 580px !important; }
                .container table {
                    width: 100% !important;
                    border-collapse: collapse; }
                .container .masthead {
                    padding: 80px 0;
                    background: #71bc37;
                    color: white; }
                    .container .masthead h1 {
                    margin: 0 auto !important;
                    max-width: 90%;
                    text-transform: uppercase; }
                .container .content {
                    background: white;
                    padding: 30px 35px; }
                    .container .content.footer {
                    background: none; }
                    .container .content.footer p {
                        margin-bottom: 0;
                        color: #888;
                        text-align: center;
                        font-size: 14px; }
                    .container .content.footer a {
                        color: #888;
                        text-decoration: none;
                        font-weight: bold; }
        """

    html = '''\
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width"/>
        <style type="text/css">
        {style}
        </style>
    </head>
    <body>
    <table class="body-wrap">
        <tr>
            <td class="container">
                <table>
                    <tr>
                        <td align="center" class="masthead">
                            <h1>Новая заявка!</h1>
                            
                        </td>
                    </tr>
                    <tr>
                        <td class="content">
                            <h2>Новая заявка с сайта</h2>
                            <p>Имя пользователя: {email.name}</p>
                            <p>Email пользователя: {email.email}</p>
                            <p>Номер телефона пользователя: {email.phone}</p>
                            <p><em>– Smile-house</em></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="container">
                <table>
                    <tr>
                        <td class="content footer" align="center">
                            <p>Sent by Smile-house, Valencia,</p>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </body>
    </html>
    '''.format(**locals())
    background_tasks.add_task(send_message_mail,settings.mail_username,html)
    return 'send pk'


app.include_router(
    email_router
    )