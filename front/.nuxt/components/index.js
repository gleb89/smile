import { wrapFunctional } from './utils'

export { default as Foot } from '../../components/Foot.vue'
export { default as Footen } from '../../components/Footen.vue'
export { default as Footes } from '../../components/Footes.vue'
export { default as Nav } from '../../components/Nav.vue'
export { default as Naven  } from '../../components/Naven .vue'
export { default as Navesp } from '../../components/Navesp.vue'
export { default as Navmobile } from '../../components/Navmobile.vue'
export { default as Navmobileen } from '../../components/Navmobileen.vue'
export { default as Navmobileesp } from '../../components/Navmobileesp.vue'
export { default as Zapesp } from '../../components/Zapesp.vue'
export { default as Zapic } from '../../components/Zapic.vue'
export { default as Zapicen } from '../../components/Zapicen.vue'

export const LazyFoot = import('../../components/Foot.vue' /* webpackChunkName: "components/foot" */).then(c => wrapFunctional(c.default || c))
export const LazyFooten = import('../../components/Footen.vue' /* webpackChunkName: "components/footen" */).then(c => wrapFunctional(c.default || c))
export const LazyFootes = import('../../components/Footes.vue' /* webpackChunkName: "components/footes" */).then(c => wrapFunctional(c.default || c))
export const LazyNav = import('../../components/Nav.vue' /* webpackChunkName: "components/nav" */).then(c => wrapFunctional(c.default || c))
export const LazyNaven  = import('../../components/Naven .vue' /* webpackChunkName: "components/naven " */).then(c => wrapFunctional(c.default || c))
export const LazyNavesp = import('../../components/Navesp.vue' /* webpackChunkName: "components/navesp" */).then(c => wrapFunctional(c.default || c))
export const LazyNavmobile = import('../../components/Navmobile.vue' /* webpackChunkName: "components/navmobile" */).then(c => wrapFunctional(c.default || c))
export const LazyNavmobileen = import('../../components/Navmobileen.vue' /* webpackChunkName: "components/navmobileen" */).then(c => wrapFunctional(c.default || c))
export const LazyNavmobileesp = import('../../components/Navmobileesp.vue' /* webpackChunkName: "components/navmobileesp" */).then(c => wrapFunctional(c.default || c))
export const LazyZapesp = import('../../components/Zapesp.vue' /* webpackChunkName: "components/zapesp" */).then(c => wrapFunctional(c.default || c))
export const LazyZapic = import('../../components/Zapic.vue' /* webpackChunkName: "components/zapic" */).then(c => wrapFunctional(c.default || c))
export const LazyZapicen = import('../../components/Zapicen.vue' /* webpackChunkName: "components/zapicen" */).then(c => wrapFunctional(c.default || c))
