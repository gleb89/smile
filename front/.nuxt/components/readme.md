# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<Foot>` | `<foot>` (components/Foot.vue)
- `<Footen>` | `<footen>` (components/Footen.vue)
- `<Footes>` | `<footes>` (components/Footes.vue)
- `<Nav>` | `<nav>` (components/Nav.vue)
- `<Naven >` | `<naven >` (components/Naven .vue)
- `<Navesp>` | `<navesp>` (components/Navesp.vue)
- `<Navmobile>` | `<navmobile>` (components/Navmobile.vue)
- `<Navmobileen>` | `<navmobileen>` (components/Navmobileen.vue)
- `<Navmobileesp>` | `<navmobileesp>` (components/Navmobileesp.vue)
- `<Zapesp>` | `<zapesp>` (components/Zapesp.vue)
- `<Zapic>` | `<zapic>` (components/Zapic.vue)
- `<Zapicen>` | `<zapicen>` (components/Zapicen.vue)
