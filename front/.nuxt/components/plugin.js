import Vue from 'vue'
import { wrapFunctional } from './utils'

const components = {
  Foot: () => import('../../components/Foot.vue' /* webpackChunkName: "components/foot" */).then(c => wrapFunctional(c.default || c)),
  Footen: () => import('../../components/Footen.vue' /* webpackChunkName: "components/footen" */).then(c => wrapFunctional(c.default || c)),
  Footes: () => import('../../components/Footes.vue' /* webpackChunkName: "components/footes" */).then(c => wrapFunctional(c.default || c)),
  Nav: () => import('../../components/Nav.vue' /* webpackChunkName: "components/nav" */).then(c => wrapFunctional(c.default || c)),
  Naven : () => import('../../components/Naven .vue' /* webpackChunkName: "components/naven " */).then(c => wrapFunctional(c.default || c)),
  Navesp: () => import('../../components/Navesp.vue' /* webpackChunkName: "components/navesp" */).then(c => wrapFunctional(c.default || c)),
  Navmobile: () => import('../../components/Navmobile.vue' /* webpackChunkName: "components/navmobile" */).then(c => wrapFunctional(c.default || c)),
  Navmobileen: () => import('../../components/Navmobileen.vue' /* webpackChunkName: "components/navmobileen" */).then(c => wrapFunctional(c.default || c)),
  Navmobileesp: () => import('../../components/Navmobileesp.vue' /* webpackChunkName: "components/navmobileesp" */).then(c => wrapFunctional(c.default || c)),
  Zapesp: () => import('../../components/Zapesp.vue' /* webpackChunkName: "components/zapesp" */).then(c => wrapFunctional(c.default || c)),
  Zapic: () => import('../../components/Zapic.vue' /* webpackChunkName: "components/zapic" */).then(c => wrapFunctional(c.default || c)),
  Zapicen: () => import('../../components/Zapicen.vue' /* webpackChunkName: "components/zapicen" */).then(c => wrapFunctional(c.default || c))
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}
