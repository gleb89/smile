import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _3e32d712 = () => interopDefault(import('../pages/dmitryi/index.vue' /* webpackChunkName: "pages/dmitryi/index" */))
const _6676441a = () => interopDefault(import('../pages/ekaterina/index.vue' /* webpackChunkName: "pages/ekaterina/index" */))
const _0f18687b = () => interopDefault(import('../pages/en/index.vue' /* webpackChunkName: "pages/en/index" */))
const _d3e724fc = () => interopDefault(import('../pages/encarna/index.vue' /* webpackChunkName: "pages/encarna/index" */))
const _548b434e = () => interopDefault(import('../pages/haver/index.vue' /* webpackChunkName: "pages/haver/index" */))
const _bd78ca36 = () => interopDefault(import('../pages/mariya/index.vue' /* webpackChunkName: "pages/mariya/index" */))
const _7de8299d = () => interopDefault(import('../pages/rostislav/index.vue' /* webpackChunkName: "pages/rostislav/index" */))
const _f5429cd6 = () => interopDefault(import('../pages/ru/index.vue' /* webpackChunkName: "pages/ru/index" */))
const _aee9e48c = () => interopDefault(import('../pages/en/dmitryi/index.vue' /* webpackChunkName: "pages/en/dmitryi/index" */))
const _c8c91e7c = () => interopDefault(import('../pages/en/ekaterina/index.vue' /* webpackChunkName: "pages/en/ekaterina/index" */))
const _0064a42a = () => interopDefault(import('../pages/en/encarna/index.vue' /* webpackChunkName: "pages/en/encarna/index" */))
const _4d5df814 = () => interopDefault(import('../pages/en/haver/index.vue' /* webpackChunkName: "pages/en/haver/index" */))
const _95942186 = () => interopDefault(import('../pages/en/mariya/index.vue' /* webpackChunkName: "pages/en/mariya/index" */))
const _99e55376 = () => interopDefault(import('../pages/en/rostislav/index.vue' /* webpackChunkName: "pages/en/rostislav/index" */))
const _0b6b9cd4 = () => interopDefault(import('../pages/ru/dmitryi/index.vue' /* webpackChunkName: "pages/ru/dmitryi/index" */))
const _6ed6bd48 = () => interopDefault(import('../pages/ru/ekaterina/index.vue' /* webpackChunkName: "pages/ru/ekaterina/index" */))
const _63453344 = () => interopDefault(import('../pages/ru/encarna/index.vue' /* webpackChunkName: "pages/ru/encarna/index" */))
const _095e9190 = () => interopDefault(import('../pages/ru/haver/index.vue' /* webpackChunkName: "pages/ru/haver/index" */))
const _f24bd63a = () => interopDefault(import('../pages/ru/mariya/index.vue' /* webpackChunkName: "pages/ru/mariya/index" */))
const _3ff2f242 = () => interopDefault(import('../pages/ru/rostislav/index.vue' /* webpackChunkName: "pages/ru/rostislav/index" */))
const _156b09d3 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/dmitryi",
    component: _3e32d712,
    name: "dmitryi"
  }, {
    path: "/ekaterina",
    component: _6676441a,
    name: "ekaterina"
  }, {
    path: "/en",
    component: _0f18687b,
    name: "en"
  }, {
    path: "/encarna",
    component: _d3e724fc,
    name: "encarna"
  }, {
    path: "/haver",
    component: _548b434e,
    name: "haver"
  }, {
    path: "/mariya",
    component: _bd78ca36,
    name: "mariya"
  }, {
    path: "/rostislav",
    component: _7de8299d,
    name: "rostislav"
  }, {
    path: "/ru",
    component: _f5429cd6,
    name: "ru"
  }, {
    path: "/en/dmitryi",
    component: _aee9e48c,
    name: "en-dmitryi"
  }, {
    path: "/en/ekaterina",
    component: _c8c91e7c,
    name: "en-ekaterina"
  }, {
    path: "/en/encarna",
    component: _0064a42a,
    name: "en-encarna"
  }, {
    path: "/en/haver",
    component: _4d5df814,
    name: "en-haver"
  }, {
    path: "/en/mariya",
    component: _95942186,
    name: "en-mariya"
  }, {
    path: "/en/rostislav",
    component: _99e55376,
    name: "en-rostislav"
  }, {
    path: "/ru/dmitryi",
    component: _0b6b9cd4,
    name: "ru-dmitryi"
  }, {
    path: "/ru/ekaterina",
    component: _6ed6bd48,
    name: "ru-ekaterina"
  }, {
    path: "/ru/encarna",
    component: _63453344,
    name: "ru-encarna"
  }, {
    path: "/ru/haver",
    component: _095e9190,
    name: "ru-haver"
  }, {
    path: "/ru/mariya",
    component: _f24bd63a,
    name: "ru-mariya"
  }, {
    path: "/ru/rostislav",
    component: _3ff2f242,
    name: "ru-rostislav"
  }, {
    path: "/",
    component: _156b09d3,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
