export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Clínica odontológica en Valencia | (Smile House)',
    htmlAttrs: {
      lang: 'es'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Ortodoncia,Odontopediatría,Blanqueamiento,Medicina estética,Cirugía e implantología,Rehabilitación oral en Valencia' },
      { name: 'format-detection', content: 'telephone=no' },
      {property: "og:image", content :"https://lh5.googleusercontent.com/p/AF1QipOI2kuaQlAvtcVTU_rLhhXvrmiYDek5jG-8XKo1=w600"},
      {name:"google-site-verification" ,content:"SviVwJBvklqjgtNBBzgUcElGB2nwhpiVGOh6naev0PQ" },
    ],
    
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/theme/index.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    // '@nuxtjs/eslint-module'
    
  ],
  

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/robots',
    '@nuxtjs/sitemap',
    '@nuxtjs/axios'
  ],
  axios: {
    // proxy: true
  },
  // gtm: {
  //   id: 'G-77S9DG7XH5'
  //  },
  server: {
    port: 80 // default: 3000
  },
  sitemap: {
    generate: true,
    hostname: 'https://valencia-dental.es',
    exclude: [
 
    ]
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
